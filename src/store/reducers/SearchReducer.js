const initialState = {
	sending_request: false,
	search_list: [
	]
}


const SearchReducer = (state=initialState, action) => {
	switch(action.type){
		case 'MAKE_SEARCH':
			return {
				...state,
				sending_request: true
			}
		case 'MAKE_SEARCH_SUCCESS':
			return {
				...state,
				sending_request: false,
			}
		case 'MAKE_SEARCH_ERROR':
			return {
				...state,
				sending_request: false,
			}
		case 'ADD_REQUEST_LIST':
			return {
				...state,
				search_list: [...state.search_list, action.new_search]
			}
		default:
			return state
	}

}

export default SearchReducer;