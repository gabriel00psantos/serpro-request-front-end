import authReducer from './AuthReducer';
import SearchReducer from './SearchReducer';

import { combineReducers } from 'redux';


const rootReducer = combineReducers({
	auth: authReducer,
	search: SearchReducer
});

export default rootReducer;