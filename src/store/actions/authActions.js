import axios from 'axios';
import toaster from "toasted-notes";
const api_endpoint = 'http://localhost:5000/';


export const login = (user) => {
	return (dispatch, getState) => {
		axios.post(`${api_endpoint}login`, user).then((response) =>{
			console.log(response.data.acess_token);
			let tokenAuth = response.data.acess_token;
			delete user.password
			dispatch({type: 'LOGIN_SUCCESS', tokenAuth, user})
			toaster.notify("Login Success !", {
					duration: 4000
				});
		}).catch((err) => {
			console.log(err);
			toaster.notify("Login Error !", {
					duration: 4000
				});
			dispatch({type: 'LOGIN_ERROR'})
		}).then(()=> {
			console.log('opora')
		})
	}
}

export const signUp = (user) => {
	return (dispatch, getState) => {
		axios.post(`${api_endpoint}cadastro`, user).then((response) =>{
			toaster.notify("Sign Up Success :o", {
					duration: 4000
				});
			console.log(response);
		}).catch((err) => {
			toaster.notify("Sign Up Error :o", {
					duration: 4000
				});
			console.log(err);
		}).then(()=> {
			console.log('opora')
		})
	}
}

export const logOut = () => {
	return (dispatch, getState) => {
		console.log('logout')
		dispatch({ type: 'LOGOUT'})
	}
}