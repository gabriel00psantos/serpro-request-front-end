import axios from 'axios';
import toaster from "toasted-notes";
const api_endpoint = "http://localhost:5000/";

export const searchRequest = (search) => {
	return (dispatch, getState) => {
		if (!search){
				console.log('sem search');
				return;
			}
		
		const { tokenAuth } = getState().auth;
		const config = {
    		headers: { Authorization: `Bearer ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_SEARCH'});
		axios.get(`${ api_endpoint}consulta/${search}`, config).then(response => {
			console.log(response);
			if (response.data.error){
				dispatch({ type: 'MAKE_SEARCH_ERROR'});
			}else {
				toaster.notify("Consulta executada com sucesso :D", {
					duration: 4000
				});
				dispatch({ type: 'MAKE_SEARCH_SUCCESS'});
			}
			const search_response = {
				status: 'status' in response.data ? response.data.status : response.data.error.reason ,
				cpf_consultado: search,
				key: 100 * Math.random(0, 99999999) + 100 * Math.random(0, 777)
			}
			dispatch({ type: 'ADD_REQUEST_LIST', new_search: search_response});

		}).catch(err => {
			toaster.notify("Requisição com erro :D", {
					duration: 4000
				});
			dispatch({ type: 'MAKE_SEARCH_ERROR'});
		})
	}

}