import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signUp } from '../../store/actions/authActions';
import { Redirect } from 'react-router-dom';

class Signup extends Component{
	state = {
		email: '',
		username: '',
		password: ''
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}

	handleSubmit = (e) => {
		e.preventDefault();
		console.log(this.state);
		this.props.getIn(this.state);
	}

	render(){
		const { isAuth } = this.props;
		if (isAuth) return <Redirect to="/"/>
		return (
			<div className="container">
				<div className="box">
				<div className="row center-align">
					<h2>Signup</h2>
				</div>
					<form className="col s12">
						<div className="row">
							<div className="input-field col s8">
								<input placeholder="" id="email" type="text" className="validate" onChange={ this.handleChange } />
								<label forhtml="email">Email</label>
							</div>
							<div className="input-field col s8">
								<input placeholder="" id="username" type="text" className="validate" onChange={ this.handleChange } />
								<label forhtml="username">Username</label>
							</div>
							<div className="input-field col s8">
								<input placeholder="" id="password" type="password" className="validate" onChange={ this.handleChange } />
								<label forhtml="password">Password</label>
							</div>
						</div>
						<div className="row">
							<button className="btn waves-effect waves-light" type="submit" onClick={ this.handleSubmit } name="action">Sign up
    							<i className="material-icons right">send</i>
  							</button>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getIn : (user) => dispatch(signUp(user))
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(Signup);