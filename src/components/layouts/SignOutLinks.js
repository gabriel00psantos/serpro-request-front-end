import React from 'react';
import { Link } from 'react-router-dom';

const SingOutLinks = () => {
	return (
		<React.Fragment>
			<li><Link to="/signin">Login</Link></li>
			<li><Link to="/signup">Sign up</Link></li>
		</React.Fragment>
	)
}

export default SingOutLinks;