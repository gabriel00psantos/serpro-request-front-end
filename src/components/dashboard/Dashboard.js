import React, { Component } from 'react';
import { connect } from 'react-redux';

class Dashboard extends Component{
	render(){
		const img = 'https://media.giphy.com/media/8Sy6PsU7oPMhq/giphy.gif'
		
		return (
			<div className="container">
				<div className="box">
					<div className="row">
						<h3 className="center-align">Desafio prático</h3>
						<div className="center-align">
							<img  alt="" src={img}/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth
	}
}

export default connect(mapStateToProps, null)(Dashboard);