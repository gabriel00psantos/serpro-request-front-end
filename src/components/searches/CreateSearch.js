import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { searchRequest } from '../../store/actions/searchActions';
import ListSearch  from './ListSearch';
import Loader from '../layouts/Loader';

class CreateSearch extends Component{
	state = {
		endpoint : 'https://devserpro.github.io/apiserpro/apis/swagger-ui-master/dist/' +
				   'index.html?bearer=4e1a1858bdd584fdc077fb7d80f39283&'+
				   'url=https://devserpro.github.io/apiserpro/apis/'+
				   'swaggers/cpf/swagger-cpf-df-trial-bsa.json#!/Consulta_CPF_DF/get_cpf_ni',
		cpf: ''
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}

	handleSubmit = (e) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		this.props.makeSearch(this.state.cpf);
	}

	render(props){
		let { isAuth } = this.props ;
		let { endpoint } = this.state;
		
		if (!isAuth) return <Redirect to='/signin'/>
		return (
			<div className="container">
				<div className="box">
					<div className="row">
					    <h3 className="center-align">Busque por um CPF </h3>
					    <form className="col s5">
			      			<p>Consulte um CPF usando como base os parametros desse <a href={endpoint}>endpoint</a></p>
					    	<div className="row">
						        <div className="input-field col s12">
						          <input placeholder="" 
						          		 id="cpf" 
						          		 type="text" className="validate"
						          		 onChange={this.handleChange} />
						          <label htmlFor="cpf">CPF</label>
						        </div>
						        <button onClick={ this.handleSubmit } className="waves-effect waves-light btn">Consultar</button>
						    </div>
				        </form>
						<div className="col s6 right">
							<div className="row">
		        				<div className="col s12">
		        					<ListSearch searches={ this.props.list }/>	
		        				</div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        	<Loader active={ this.props.sending_request }/>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth,
		sending_request: state.search.sending_request,
		list: state.search.search_list
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		makeSearch: (search) => dispatch(searchRequest(search))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSearch);