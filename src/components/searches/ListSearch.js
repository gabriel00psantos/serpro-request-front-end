import React from 'react'
import SearchDetails from './SearchDetails';

const ListSearch = (props) =>{
	return (
		<React.Fragment>
			<p>Consultas já realizadas: </p>
			<div className="collection">
				{
					props.searches && props.searches.map(search => {
						return (
							<SearchDetails key={search.key} search={search}/>
						)
					}) 
				}

			</div>
		</React.Fragment>
	)
}

export default ListSearch;