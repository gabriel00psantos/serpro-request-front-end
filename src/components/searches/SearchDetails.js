import React from 'react';

const SearchDetail = ({ search }) => {
	return(
		<React.Fragment>
			<a href="#!" className="collection-item">
			<span className="badge">{search.status}</span>
			{search.cpf_consultado} 
			</a>
		</React.Fragment>
	)
}

export default SearchDetail;